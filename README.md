# Apuntes para la asignatura 'Técnicas de Ciencia de Datos'

[Estos apuntes](https://alayzappala.gitlab.io/apuntes-ciencia-datos/) hacen un repaso a la estadística descriptiva y la probabilidad desde un nivel básico durante los cinco primeros capítulos. Los siguientes capítulos se salen de esa rama de las matemáticas para abordar otros aspectos del análisis de datos como son el análisis de redes y la minería de textos. 

Así, estos textos no pretenden ser un compendio exaustivo de fórmulas matemáticas y sus desarrollos, sino dar una explicación a los conceptos, métodos y casos de uso ayudados por la algoritmia proporcionada por una de las herramientas más extendidas para el análisis de datos como es el lenguaje de programación R, así como de la interfaz para creación de documentación llamada RStudio, con la que se han redactado y editado estos apuntes.

Tampoco se tratan temas como la obtención y limpieza de datos, aspectos que se estudian en otros módulos, sino que se centran en los métodos de análisis y representación de datos más adecuados según el caso y los tipos de datos de que se disponen, para dar al alumno conocimientos de base, sin tener que supeditarse a una herramienta en concreto.

Se trata sobretodo de proporcionar un vocabulario y una comprensión de conceptos suficiente para abordar textos y documentación técnica con la seguridad de que se entiende lo que se lee, así como de dar capacidad para documentar de la manera más clara y concisa sus futuros análisis de manera que puedan ser replicados sin ambigüedad.

La idea central de lo que llaman Ciencia de Datos es que es una ciencia y, como tal, se basa en la observación y análisis normalizado de resultados reproducibles. Es el elemento común a toda ciencia, que por definición se fundamenta en los datos obtenidos con metodologías replicables y se expresa en un lenguaje común para poder ser corroborada y compartida con el resto de la comunidad científica.